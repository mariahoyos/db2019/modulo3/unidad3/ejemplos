﻿-- (1) Procedimiento que devuelva el mayor de dos números

-- con la instrucción if

  CREATE OR REPLACE PROCEDURE mayorA(a int, b int)
    BEGIN
      DECLARE resultado int;
      IF a>=b THEN 
        SET resultado=a;
      ELSE 
        SET resultado=b;
      END IF;
      SELECT resultado;
    END;

  CALL mayor(4,8);

  CREATE FUNCTION mayor(a int, b int)
    RETURNS int
    BEGIN
      DECLARE resultado int;
      IF a>=b THEN 
        SET resultado=a;
      ELSE 
        SET resultado=b;
      END IF;
      RETURN resultado;
    END;

    SELECT mayor(99,111);

-- (1b) Procedimiento almacenado que reciba dos número e indique el mayor de ellos. Realizar con consulta de totales y una tabla auxiliar

   
    CREATE PROCEDURE mayorB(IN a int,IN b int)
     BEGIN
       CREATE TEMPORARY TABLE numeros(
         id_numero int AUTO_INCREMENT,
         numero int,
         PRIMARY KEY(id_numero)   
      );
      INSERT INTO numeros (numero) VALUES (a),(b);
      SELECT MAX(numero) FROM numeros;
    END;
    
  
-- (1b_copia) Procedimiento almacenado que reciba dos número e indique el mayor de ellos. Realizar con consulta de totales y una tabla auxiliar


CREATE PROCEDURE ej1v2(IN n1 int, IN n2 int)
BEGIN 
  CREATE OR REPLACE TEMPORARY TABLE ej1(
    id int AUTO_INCREMENT,
    num int,
    PRIMARY KEY(id)
  );
  INSERT INTO ej1 (num) VALUES (n1),(n2);
  SELECT
    MAX(num)
  FROM ej1;
END;

-- (1c) Procedimiento que reciba dos números e indique el mayor de ellos con la función GREATEST

CREATE PROCEDURE mayorGreatest(a int, b int)
  BEGIN 
    SELECT GREATEST(a,b);
  END;

CALL mayorGreatest(324,23423);

-- (2) Reciba tres números y te indique el mayor de ellos. Realizarlo con instrucción if

-- (2a) if

CREATE PROCEDURE mayor3a(a int, b int, c int)
  BEGIN
    DECLARE x, y, z, resultado int;
    SET resultado=a;
      IF b>a THEN SET resultado=b;
        ELSEIF c>a THEN SET resultado=c;
      END IF;
    SELECT resultado;
  END;

-- (2b) tabla temporal y una consulta de totales

CREATE PROCEDURE mayor3b( a int,  b int, c int)
  BEGIN
    CREATE TEMPORARY TABLE tmp(
    id_tmp int AUTO_INCREMENT PRIMARY KEY,
    num int
    );
    INSERT INTO tmp (num)
  VALUES (a),(b),(c);
  SELECT MAX(num) FROM tmp;
  END;


-- (2c) greatest

CREATE PROCEDURE mayor3c(a int, b int, c int)
  BEGIN
    SELECT GREATEST(a,b,c);
  END;

-- (3) Realizar un procedimiento almacenado que reciba tres números y dos argumentos de tipo salida donde devuelva el número más grande y el número más pequeño de los tres números pasados

CREATE PROCEDURE mayor_y_menor(a int, b int, c int)
  BEGIN
    CREATE TEMPORARY TABLE tmp(
      id int AUTO_INCREMENT PRIMARY KEY,
      num int
    );
    INSERT INTO tmp (num) VALUES (a),(b),(c);
    SELECT MAX(num)máximo, MIN(num)mínimo FROM tmp;
  END;

-- (4) Realizar un procedimiento almacenado que reciba dos fechas y te muestre el número de días de diferencia entre las dos fechas

CREATE PROCEDURE diferencia_dias(a date, b date)
    BEGIN
     SELECT DATEDIFF(a,b);
    END;

CALL diferencia_dias('2010/1/10','2010/1/1');

-- (5) Realizar un procedimiento almacenado que reciba dos fechas y te muestra el número de meses de diferencia entre las dos fechas

CREATE PROCEDURE diferencia_meses(a date, b date)
  BEGIN
    SELECT PERIOD_DIFF(a,b);
  END;

CALL diferencia_meses('2010/1/1','2010/1/10');

-- (6) Realizar un procedimiento almacenado que reciba dos fechas y te devuelva en 3 argumentos de salida los días, meses y años entre las dos fechas

CREATE PROCEDURE diferencia_fechas_arg_salida(IN a date,IN b date,OUT d int,OUT m int,OUT an int)
  BEGIN
    SET d = TIMESTAMPDIFF(DAY,a, b);
    SET m = TIMESTAMPDIFF(MONTH, a,b);
    SET an = TIMESTAMPDIFF(year,a,b);
  END;

CALL diferencia_fechas_arg_salida('2010/1/1','2020/1/1',@d,@m,@a);
SELECT @d, @m, @a;

-- (7) Realizar un procedimiento almacenado que reciba una frase y te muestre el número de caracteres

CREATE PROCEDURE longitud_frase(a varchar(120))
  BEGIN 
    SELECT CHAR_LENGTH(a);
  END;

CALL longitud_frase('ejemplo');