﻿-- (1) Realizar un procedimiento almacenado que reciba un texto y un carácter. Debe indicarte si ese carácter está en el texto. Debéis localizarlo con: locate, position
DROP PROCEDURE esta_en;
CREATE PROCEDURE esta_en(texto varchar(100),caracter char(1))
  BEGIN
    DECLARE resultado int;
    DECLARE esta varchar(10);
    SET resultado = LOCATE(caracter, texto);
    IF resultado<>0 THEN SET esta = 'Sí está';
      ELSE SET esta = 'No está';
    END IF;
    SELECT esta;
  END;

CALL esta_en('text','x');

-- (2) Realizar un procedimiento almacenado que reciba un texto y un carácter. 

CREATE PROCEDURE texto_antes_de(texto varchar(100), caracter char(1))
  BEGIN 
    SELECT SUBSTRING_INDEX(texto,caracter,1);
  END;

CALL texto_antes_de('texto','x');

-- (3) Realizar un procedimiento almacenado que reciba tres número y dos argumentos de tipo salida donde devuelva el número más grande y el número más pequeño de los tres números pasados

CREATE PROCEDURE masgrande_maspequenho(a int, b int, c int,OUT pequenho int,OUT grande int)
  BEGIN
    SET pequenho = LEAST(a,b,c);
    SET grande = GREATEST(a,b,c);
  END;

CALL masgrande_maspequenho(2312,2342,123,@pequenho,@grande);
SELECT @pequenho,@grande;

-- (4) Realizar un procedimiento almacenado que muestre cuantos numeros1 y numeros2 son mayores que 50

  CREATE PROCEDURE mayores50()
    BEGIN
    DECLARE a int;
    DECLARE b int;
    SELECT COUNT(*) INTO a FROM datos WHERE numero1>50;
    SELECT COUNT(*) INTO b FROM datos WHERE numero2>50;
    SELECT a+b;
    END;

  CALL mayores50();

-- (5) Realizar un procedimiento almacenado que calcule la suma y la resta de numero1 y numero2

  CREATE OR replace PROCEDURE suma_resta_numero1_numero2()
    BEGIN
      UPDATE datos 
        SET datos.suma=datos.numero1+numero2;
      UPDATE datos
        SET datos.resta=datos.numero1-numero2;
    END;

  CALL suma_resta_numero1_numero2();
  SELECT * FROM datos;

  -- (6) Realizar un procedimiento almacenado que primero ponga todos los valores de suma y resta a null y 
  -- después calcule la suma solamente si el número1 es mayor que el numero2 y 
  -- calcula la resta de numero2-numero1 si el numero2 es mayor que el numero1

CREATE OR REPLACE PROCEDURE suma_resta_num1_num2_2()
  BEGIN
    -- primero se pone suma y resta a null
    UPDATE datos
       SET datos.suma = NULL, datos.resta = NULL;

    -- update de suma y resta
    UPDATE datos
      SET datos.suma=numero1+numero2
        WHERE numero1>numero2;
    UPDATE datos
      SET datos.resta = IF(numero1>numero2,numero1-numero2,numero2-numero1);     
    
  END;

CALL suma_resta_num1_num2_2();
SELECT * FROM datos;

-- (7) Realizar un procedimiento almacenado que coloque en el campo junto el texto1, texto2

CREATE PROCEDURE concatenar()
  BEGIN
    UPDATE datos
      SET junto = CONCAT_WS(' ',texto1,texto2);
  END;

CALL concatenar();
SELECT * FROM datos;

-- (8) Realizar un procedimiento almacenado que coloque en el campo "junto" el valor NULL
-- Después debe colocar en el campo junto el texto 1-texto2 si el rango es A
-- Si el rango es B debe colocar texto1+texto2
-- Si el rango es distinto debe colocar texto1 nada más

CREATE PROCEDURE junto()
  BEGIN
      UPDATE datos
      SET datos.junto = NULL;
    
      UPDATE datos
        SET junto=IF(rango='A', CONCAT(texto1,'-',texto2),IF(rango='B',CONCAT(texto1,'+',texto2),texto1));
  END;

CALL junto();
SELECT * FROM datos;