﻿-- (2) Función área triángulo

DROP FUNCTION areaTriangulo;
CREATE FUNCTION areaTriangulo(base int, altura int)
    RETURNS float
  BEGIN
    DECLARE AREA float;
    set area=base*altura/2;
    RETURN AREA;
  END;

-- (3) Función perímetro triángulo

CREATE FUNCTION perimetroTriangulo(base int,lado2 int,lado3 int)
  RETURNS int
  BEGIN
    DECLARE perimetro int;
    SET perimetro = base+lado2+lado3;
    RETURN perimetro;
  END;

SELECT * FROM triangulos;

-- (4) Procedimiento almacenado que cuando le llames como argumentos: id1: id inicial id2: id final actualice el área y el perímetro de los triángulos (utilizando
-- las funciones realizadas) que estén comprendidos entre los id pasados

CREATE OR REPLACE PROCEDURE actualizarTriangulos(id1 int, id2 int)
  BEGIN
    UPDATE triangulos
      SET 
        area = areaTriangulo(base,altura),
        perimetro = perimetroTriangulo(base,lado2,lado3)
     WHERE id BETWEEN id1 AND id2;
  END;

SELECT * FROM triangulos;

CALL actualizarTriangulos(1,19);

-- (5) Función area 

CREATE FUNCTION areaCuadrado(lado int)
  RETURNS int
  BEGIN 
    DECLARE area int;
    SET area = POW(lado,2);
    RETURN area;
  END;

-- (6) Función perímetro 

CREATE FUNCTION perimetroCuadrado(lado int)
  RETURNS int
  BEGIN 
    DECLARE perimetro int;
    SET perimetro = lado * 4;
    RETURN perimetro;
  END;

SELECT DISTINCT perimetroCuadrado(2) FROM cuadrados;

-- (7) Procedimiento almacenado cuadrados(id1 int, id2 int)

SELECT * FROM cuadrados;

CREATE PROCEDURE cuadrados(id1 int, id2 int)
  BEGIN 
    UPDATE cuadrados
        set
          area = areaCuadrado(lado),
          perimetro = perimetroCuadrado(lado)
      WHERE ID BETWEEN id1 AND id2;
  END;

CALL cuadrados(1,19);
