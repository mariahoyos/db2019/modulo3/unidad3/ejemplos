﻿-- Crea un procedimiento simple que muestre por pantalla el texto "esto es un ejemplo de procedimiento" (donde la cabecera de la columna sea "mensaje")

CREATE OR REPLACE TABLE alumnos(
  id int(11) NOT NULL,
  nota1 int(11) NOT NULL,
  nota2 int(11) NOT NULL,
  nota3 int(11) NOT NULL,
  nota4 int(11) NOT NULL,
  grupo int(11) NOT NULL,
  media varchar(255) DEFAULT NULL,
  max varchar(255) DEFAULT NULL,
  min varchar(255) DEFAULT NULL,
  moda varchar(255) DEFAULT NULL,
  PRIMARY KEY(id)
  );

SELECT * FROM alumnos;
SELECT * FROM circulo;


ALTER TABLE alumnos
  ADD INDEX grupo_index(grupo);
ALTER TABLE alumnos
  ADD INDEX nota1_index(nota1);
ALTER TABLE alumnos
  ADD INDEX nota2_index(nota2);
ALTER TABLE alumnos
  ADD INDEX nota3_index(nota3);
ALTER TABLE alumnos
  ADD INDEX nota4_index(nota4);

CREATE OR REPLACE TABLE circulo(
  id int(11) NOT NULL,
  radio int(11) NOT NULL,
  tipo varchar(5) DEFAULT NULL,
  area varchar(255) DEFAULT NULL,
  perimetro varchar(255) DEFAULT NULL,
  PRIMARY KEY(id)
);

CREATE OR REPLACE TABLE conversion(
  id int(11) NOT NULL,
  cm double DEFAULT NULL,
  m double DEFAULT NULL,
  km double DEFAULT NULL,
  pulgadas double DEFAULT NULL,
  PRIMARY KEY(id)
  );

CREATE OR REPLACE TABLE cuadrados(
  id int(11) NOT NULL,
  lado int(11) NOT NULL,
  area varchar(255) NOT NULL,
  perimetro varchar(255) NOT NULL,
  PRIMARY KEY(id)
);

CREATE OR REPLACE TABLE grupos(
  id int(11) NOT NULL,
  media varchar(255) NOT NULL,
  max varchar(255) NOT NULL,
  min varchar(255) NOT NULL,
  PRIMARY KEY(id)
);

CREATE OR REPLACE TABLE rectangulo(
  id int(11) NOT NULL,
  lado1 int(11) NOT NULL,
  lado2 int(11) NOT NULL,
  area varchar(255) DEFAULT NULL,
  perimetro varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
);

CREATE OR REPLACE TABLE triangulos(
  id int(11) NOT NULL,
  base int(11) NOT NULL,
  altura int(11) NOT NULL,
  lado2 int(11) NOT NULL,
  lado3 int(11) NOT NULL,
  area varchar(255) DEFAULT NULL,
  perimetro varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
);